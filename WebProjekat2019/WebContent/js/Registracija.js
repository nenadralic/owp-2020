$(document).ready(function() {
	var usernameInput = $('#usernameInput');
	var passwordInput = $('#passwordInput');

	$('#registerSubmit').on('click', function(event) {
		var username = usernameInput.val().trim();
		var password = passwordInput.val().trim();

		if(username == ""){
			alert('Morate uneti korisnicko ime!');
			event.preventDefault();
			return false;
		}
		else if(password == ""){
			alert('Morate uneti lozinku!');
			event.preventDefault();
			return false;
		}
		
		
		var params = {
			'username': username, 
			'password': password
		}
		$.post('RegistracijaServlet', params, function(data) {

			if (data.status == 'fail') {
				alert('Korisnicko ime je zauzeto!');
				usernameInput.val('');
				passwordInput.val('');
				return;
			}
			if (data.status == 'success') {
				window.location.replace('Login.html');
			}
		});

		event.preventDefault();
		return false;
	});
});