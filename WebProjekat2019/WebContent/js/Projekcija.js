$(document).ready(function() {	
	
	var idProjekcije = window.location.search.slice(1).split('&')[0].split('=')[1];
	
	$('#karteTabela').hide();
	$('#karteZaProjekciju').hide();
	
    function ispisProfil(){
		$.get('ProjekcijaServlet', function(data){
			
			if (data.status == 'unauthenticated') {
				window.location.replace('Login.html');
				return;
			}

			if (data.status == 'success') {
				var username = data.prijavljenKorisnikKorime;
                $('#profil').append('<a class="nav-link" href="Profil.html?username='+ username +'">Profil</a>');
			}
			
		});
	}
    
    ispisProfil();
    
	$('#odjava').on('click', function(event) {
		$.get('LogoutServlet', function(data) {

			if (data.status == 'unauthenticated') {
				window.location.replace('Index.html');
				return;
			}
		});

		event.preventDefault();
		return false;
	});
	
	function getProjekcija(){
		$.get('ProjekcijaServlet', {'idProjekcije': idProjekcije}, function(data) {
			if (data.status == 'unauthenticated') {
				window.location.replace('Login.html');
				return;
			}
			
			if (data.status == 'success') {
				var projekcija = data.projekcija;
				
				var filmovi = data.filmovi;
				var sale = data.sale;
				var tipovi = data.tipoviProjekcije;
				
				document.title = "Projekcija - " + projekcija.datumIVreme;
				
				if (data.ulogaPrijavljenogKorisnika == 'KORISNIK') {
					
					$('#korisnikPrikaz').append(
							'<div class="tabelaForma" style="width: 40%; margin-top: 250px; position: relative;">'+
							'<table class="table table-dark">' +
								'<tr id="nazivFilma"></tr>' +
								'<tr><td align="left">Datum i vreme prikazivanja:</td><td align="left">'+ projekcija.datumIVreme +'</td></tr>' +
								'<tr id="nazivTipaProjekcije"></tr>' +
								'<tr id="nazivSale"></tr>' +
								'<tr><td align="left">Cena karte:</td><td align="left">'+ projekcija.cena +'</td></tr>' +
								'<tr><td/><td align="left"; id="dugmeTd"></td></tr>' +
							'</table>' +			
						'</div>'		
					);
					
					for(f in filmovi){
						if(filmovi[f].id == projekcija.film){
							$('#nazivFilma').append(
							'<td align="left">Film:</td><td align="left"><a href="Film.html?id=' + projekcija.film + '">' + filmovi[f].naziv + '</td>'
							);
							break;
						}			
					}
					
					for(t in tipovi){
						if(tipovi[t].id == projekcija.tipProjekcije){
							$('#nazivTipaProjekcije').append(
							'<td align="left">Tip projekcije:</td><td align="left">'+ tipovi[t].naziv +'</td>'
							);
							break;
						}			
					}
					
					for(s in sale){
						if(sale[s].id == projekcija.sala){
							$('#nazivSale').append(
							'<td align="left">Sala:</td><td align="left">' + sale[s].naziv + '</td>'
							);
							break;
						}			
					}
					
					if(Date.parse(projekcija.datumIVreme) > new Date() && projekcija.aktivan){
						$('#korisnikPrikaz').append(
								'</br>' +
								'<p><a style="text-decoration: none; font-weight: bold; color: dodgerblue;" href="KupovinaKarteFaza2.html?id='+ idProjekcije +'">Kupi kartu</a></p>'
						);
					}
					

				
				}else if (data.ulogaPrijavljenogKorisnika == 'ADMIN') {

	                $('#adminInterfejs').append('<li><a href="UpravljanjeKorisnicima.html" class="nav-link" style="width:180px" >Upravljanje korisnicima</a></li>');
	                $('#adminInterfejs').append('<li><a href="Izvestavanje.html" class="nav-link"" >Izvestavanje</a></li>');

					$('#adminPrikaz').append(
							'<div class="tabelaForma" style="width: 40%; margin-top: 250px; position: relative;">'+
							'<table class="table table-dark">' +
								'<tr id="nazivFilma"></tr>' +
								'<tr><td align="left">Datum i vreme prikazivanja:</td><td align="left">'+ projekcija.datumIVreme +'</td></tr>' +
								'<tr id="nazivTipaProjekcije"></tr>' +
								'<tr id="nazivSale"></tr>' +
								'<tr><td align="left">Cena karte:</td><td align="left">'+ projekcija.cena +'</td></tr>' +
								'<tr><td/><td align="left"; id="dugmeTd"></td></tr>' +
							'</table>' +			
						'</div>'
					);
					
					$('#karteZaProjekciju').show();
					
					var karte = data.karteZaProjekciju;
					var projekcije = data.projekcije;
					for (k in karte) {
						$('#karteTabela').append(
							'<tr>' + 
								'<td style="background-color : lightblue;"><a href="Karta.html?id=' + karte[k].id + '">' + karte[k].datumIVreme + '</a></td>' + 
								'<td style="background-color : lightblue;"><a href="Nalog.html?username=' + karte[k].korisnik + '">' + karte[k].korisnik +'</a></td>' +
							'</tr>' 
						);
					
					}
					
					$('#karteTabela').show();
					
					for(f in filmovi){
						if(filmovi[f].id == projekcija.film){
							$('#nazivFilma').append(
							'<td align="left">Film:</td><td align="left"><a href="Film.html?id=' + projekcija.film + '">' + filmovi[f].naziv + '</td>'
							);
							break;
						}			
					}
					
					for(t in tipovi){
						if(tipovi[t].id == projekcija.tipProjekcije){
							$('#nazivTipaProjekcije').append(
							'<td align="left">Tip projekcije:</td><td align="left">'+ tipovi[t].naziv +'</td>'
							);
							break;
						}			
					}
					
					for(s in sale){
						if(sale[s].id == projekcija.sala){
							$('#nazivSale').append(
							'<td align="left">Sala:</td><td align="left">' + sale[s].naziv + '</td>'
							);
							break;
						}			
					}
					
					if(projekcija.aktivan){
						$('#dugmeTd').append(
								'<input class="btn btn-danger" type="submit" value="Obrisi" id="brisanjeSubmit">'
						);
					}
					
					}
					
					$('#brisanjeSubmit').on('click', function(event) {
							$.post('ProjekcijaServlet', {'idProjekcije': idProjekcije}, function(data) {
								if (data.status == 'unauthenticated') {
									window.location.replace('Login.html');
									return;
								}

								if (data.status == 'success') {
									window.location.replace('KorisnikGlavna.html');
									return;
								}
							});

							event.preventDefault();
							return false;
						});
					
				}
			
		});
}
	getProjekcija();
	
});