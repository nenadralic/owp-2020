$(document).ready(function() {	
	
	var idFilma = window.location.search.slice(1).split('&')[0].split('=')[1];
	
    function ispisProfil(){
		$.get('KupovinaKarteServlet', function(data){
			
			if (data.status == 'unauthenticated') {
				window.location.replace('Login.html');
				return;
			}

			if (data.status == 'success') {
				var username = data.prijavljenKorisnikKorime;
                $('#profil').append('<a class="nav-link" href="Profil.html?username='+ username +'">Profil</a>');
			}
			
		});
	}

ispisProfil();
	
	$('#odjava').on('click', function(event) {
		$.get('LogoutServlet', function(data) {

			if (data.status == 'unauthenticated') {
				window.location.replace('Index.html');
				return;
			}
		});

		event.preventDefault();
		return false;
	});
	
	function getProjekcije(){
		$.get('KupovinaKarteServlet', function(data) {
			if (data.status == 'unauthenticated') {
				window.location.replace('Login.html');
				return;
			}
			
			if (data.status == 'success') {
				
				var projekcije = data.projekcije;
				var filmovi = data.filmovi;
				var sale = data.sale;
				var tipovi = data.tipoviProjekcije;
				
				for(f in filmovi){
					if(filmovi[f].id == idFilma){
						$('#imeFilma').append('Projekcije za film - ' + filmovi[f].naziv);
						break;
					}
				}
								
				for (p in projekcije) {
					$('#projekcijeTabela').append(
						
							'<tr>' + 
								'<td><a href="Projekcija.html?id=' + projekcije[p].id + '">' + projekcije[p].datumIVreme + '</a></td>' + 
								'<td>' + tipovi.find(x => x.id === projekcije[p].tipProjekcije).naziv + '</td>' + 
								'<td>' + sale.find(x => x.id === projekcije[p].sala).naziv + '</td>' + 
								'<td>' + projekcije[p].cena + '</td>' +  
								'<td><a href="KupovinaKarte2.html?id=' + projekcije[p].id + '">' + 'Kupi kartu' + '</td>' +
							'</tr>' 
					);
				}
				
			}
			
			
		});
	}
	
	getProjekcije();
	
});