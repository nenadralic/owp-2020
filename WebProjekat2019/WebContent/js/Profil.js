$(document).ready(function() {	
	
    var korime = window.location.search.slice(1).split('&')[0].split('=')[1];
    $('#divTabelaKarte').hide();
    
    $('#odjava').on('click', function(event) {
        $.get('LogoutServlet', function(data) {
    
            if (data.status == 'unauthenticated') {
                window.location.replace('Index.html');
                return;
            }
        });
    
        event.preventDefault();
        return false;
    });
    
    function getKorisnik(){
        $.get('ProfilServlet', {'korime': korime}, function(data) {
            if (data.status == 'unauthenticated') {
                window.location.replace('Login.html');
                return;
            }
            
            if (data.status == 'success') {
                var korisnik = data.korisnik;
                
                document.title = "Nalog - " + korime;
                $('#imeNaloga').append('Pregled profila - ' + korime);
                
                if (data.ulogaPrijavljenogKorisnika == 'KORISNIK') {
                    
                    $('#korisnikPrikaz').append(
                        '<div class="tabelaForma" style="width: 40%; margin-top: 250px; position: relative;">'+
                        '<table class="table table-dark">' +
                                '<tr><td align="left">Korisnicko ime:</td><td align="left">' + korime + '</td></tr>' +
                                '<tr><td align="left">Datum i vreme registracije:</td><td align="left">'+ korisnik.datumReg +'</td></tr>' +
                                '<tr><td align="left">Uloga:</td><td align="left">'+ korisnik.uloga +'</td></tr>' +
                                '<tr><td/><td align="left"><button class="btn btn-primary" type="button" id="izmenaButton">Izmeni</button></td></tr>' +
                            '</table>' +
                        '</div>'			
                    );
                    
                    var karte = data.karteKorisnika;
                    var projekcije = data.projekcije;
                    var filmovi = data.filmovi;
                    console.log(karte);
                    console.log(projekcije);
                    console.log(filmovi);
                    for (k in karte) {
                    	console.log(karte[k].projekcija);
                        $('#karteTabela').append(
                        		 '<tr>' + 
//                                 '<td style="background-color : lightblue;">' + filmovi.find(x => x.id === projekcije.find(x => x.id === karte[k].projekcija).film).naziv +'</td>' +
                                '<td style="background-color : lightblue;"><a href="Karta.html?id=' + karte[k].id + '">' + karte[k].datumIVreme + '</a></td>' + 
                            '</tr>' 
                        );
                    
                    }
                    
                    $('#divTabelaKarte').show();
                    
                    $('#izmenaButton').on('click', function(event) {
                        $('#korisnikIzmena').append(
                            '<div class="tabelaForma" style="margin-top:250px; margin-bottom:1600px">' +
                            '<p>Izmena lozinke</p>' +
                                '<table>' +
                                        '<tr><td>Trenutna lozinka:</td><td><input type="password" name="currentPassword" id="currentPasswordInput"/></td></tr>' +
                                        '<tr><td>Lozinka:</td><td><input type="password" name="password" id="passwordInput"/></td></tr>' +
                                        '<tr><td/><td align="left"><input class="btn btn-success" type="submit" value="Potvrdi" id="izmenaPotvrdaSubmitKorisnik"></td></tr>' +
                                    '</table>' +
                                '</div>'
                        
                        );
                        
                        var trenutnaLozinkaInput = $('#currentPasswordInput');
                        var novaLozinkaInput = $('#passwordInput');
                        
                        $('#izmenaPotvrdaSubmitKorisnik').on('click', function(event) {
                            var trenutnaLozinka = trenutnaLozinkaInput.val();
                            var novaLozinka = novaLozinkaInput.val();
                            var uloga = 'KORISNIK';
                            
                            console.log(trenutnaLozinka + novaLozinka);
                            
                            if(trenutnaLozinka == ""){
                                alert("Morate uneti trenutnu lozinku!");
                                event.preventDefault();
                                return false;
                            }
                            else if(novaLozinka == ""){
                                alert("Morate uneti novu lozinku!");
                                event.preventDefault();
                                return false;
                            }
                            else if (!novaLozinka.match(/^[0-9a-z]+$/i)){
                                alert('Lozinka moze sadrzati samo slova i brojeve bez razmaka!');
                                event.preventDefault();
                                return false;
                            }
                            else if(trenutnaLozinka !== korisnik.lozinka){
                                alert("Uneli ste pogresnu trenutnu lozinku!");
                                event.preventDefault();
                                return false;
                            }
                            else if(novaLozinka == trenutnaLozinka){
                                alert("Nova lozinka se ne sme podudarati sa trenutnom!");
                                event.preventDefault();
                                return false;
                            }
                            
                            params = {
                                    'action': 'izmenaKorisnika',
                                    'korimeKorisnik': korime,
                                    'lozinkaKorisnik': novaLozinka,
                                    'uloga' : uloga,
                                };
                            
                            $.post('ProfilServlet', params, function(data) {
                                if (data.status == 'unauthenticated') {
                                    window.location.replace('Login.html');
                                    return;
                                }
    
                                if (data.status == 'success') {
                                    window.location.replace('KorisnikProjekcije.html');
                                    return;
                                }
                            });
    
                            event.preventDefault();
                            return false;
    
                        });
                        
                    });
                
                
                }else if (data.ulogaPrijavljenogKorisnika == 'ADMIN') {
                    var prijavljenKorisnik = data.prijavljenKorisnik;
                    console.log(prijavljenKorisnik);
                    $('#adminInterfejs').append('<li><a href="UpravljanjeKorisnicima.html" class="nav-link" style="width:180px" >Upravljanje korisnicima</a></li>');
                    $('#adminInterfejs').append('<li><a href="Izvestavanje.html" class="nav-link"" >Izvestavanje</a></li>');
                    if(prijavljenKorisnik == korime){
                    $('#adminPrikaz').append(
                    		'<div class="tabelaForma" style="width: 40%; margin-top: 250px; position: relative;">'+
                            '<table class="table table-dark">' +
                                '<tr><td align="left">Korisnicko ime:</td><td align="left">' + korime + '</td></tr>' +
                                '<tr><td align="left">Datum i vreme registracije:</td><td align="left">'+ korisnik.datumReg +'</td></tr>' +
                                '<tr><td align="left">Uloga:</td><td align="left">'+ korisnik.uloga +'</td></tr>' +
                                '<tr><td/><td align="left"><button class="btn btn-primary" type="button" id="izmenaButtonAdmin">Izmeni</button></td></tr>' +
                            '</table>' +
                        '</div>'			
                    );
                    }else{
                        $('#adminPrikaz').append(
                        		'<div class="tabelaForma" style="width: 40%; margin-top: 250px; position: relative;">'+
                                '<table class="table table-dark">' +
                                    '<tr><td align="left">Korisnicko ime:</td><td align="left">' + korime + '</td></tr>' +
                                    '<tr><td align="left">Datum i vreme registracije:</td><td align="left">'+ korisnik.datumRegistracije +'</td></tr>' +
                                    '<tr><td align="left">Uloga:</td><td align="left">'+ korisnik.uloga +'</td></tr>' +
                                    '<tr><td/><td align="left"><button class="btn btn-primary" type="button" id="izmenaButtonAdmin">Izmeni</button><input class="btn btn-danger" type="submit" value="Obrisi" id="brisanjeSubmit"></td></tr>' +
                                '</table>' +
                            '</div>'			
                        );
                        
                        var karte = data.karteKorisnika;
                        var projekcije = data.projekcije;
                        var filmovi = data.filmovi;
                        for (k in karte) {
                            $('#karteTabela').append(
                                    '<tr>' + 
                                    '<td style="background-color : lightblue;">' + filmovi.find(x => x.id === projekcije.find(x => x.id === karte[k].projekcija).film).naziv +'</td>' +
                                    '<td style="background-color : lightblue;"><a href="Karta.html?id=' + karte[k].id + '">' + karte[k].datumIVreme + '</a></td>' + 
                                '</tr>' 
                            );
                        
                        }
                        
                        $('#divTabelaKarte').show();
                        
                    }
                    
                    $('#izmenaButtonAdmin').on('click', function(event) {
                        if(prijavljenKorisnik == korime){
                        $('#adminIzmena').append(
                                '<div class="tabelaForma" style="margin-top:250px; margin-bottom:1600px">' +
                                '<p>Izmena lozinke i tipa korisnika</p>' +
                                    '<table>' +
                                        '<tr><td>Trenutna lozinka:</td><td><input type="password" name="currentPassword" id="currentPasswordInputAdmin"/></td></tr>' +
                                        '<tr><td>Lozinka:</td><td><input type="password" name="password" id="passwordInputAdmin"/></td></tr>' +
                                        '<tr><td>Tip Korisnika:</td><td><select id="tipKorisnika" name="comboBoxTipKorisnika">' +
                                            '<option value="3" selected="selected">Izaberi zeljeni tip</option>' +
                                            '<option value="1">KORISNIK</option>' +
                                            '<option value="2">ADMIN</option>' +		
                                        '</select></td></tr>' +
                                        '<tr><td/><td align="left"><input class="btn btn-success" type="submit" value="Potvrdi" id="izmenaPotvrdaSubmitAdmin"></td></tr>' +
                                    '</table>' +
                                '</div>'
                        
                        );
                        
                        var trenutnaLozinkaInput = $('#currentPasswordInputAdmin');
                        var novaLozinkaInput = $('#passwordInputAdmin');
                        
                        
                        $('#izmenaPotvrdaSubmitAdmin').on('click', function(event) {
                            var trenutnaLozinka = trenutnaLozinkaInput.val();
                            var novaLozinka = novaLozinkaInput.val();
                            var tipKorisnika = $('#tipKorisnika option:selected').text();
                            
                            //console.log(trenutnaLozinka + novaLozinka);
                            
                            if(novaLozinka == "" && trenutnaLozinka == ""){
                                if(tipKorisnika == "Izaberi zeljeni tip"){
                                    alert('Izaberite zeljeni tip korisnika');
                                    event.preventDefault();
                                    return false;
                                }
                                novaLozinka = korisnik.lozinka;
                                params = {
                                        'action': 'izmenaKorisnika',
                                        'korimeKorisnik': korime,
                                        'lozinkaKorisnik': novaLozinka,
                                        'uloga' : tipKorisnika,
                                    };
                                
                                $.post('ProfilServlet', params, function(data) {
                                    if (data.status == 'unauthenticated') {
                                        window.location.replace('Login.html');
                                        return;
                                    }
    
                                    if (data.status == 'success') {
                                        window.location.replace('KorisnikProjekcije.html');
                                        return;
                                    }
                                });
                                event.preventDefault();
                                return false;
                            }
                            else{
                            if (!novaLozinka.match(/^[0-9a-z]+$/i)){
                                alert('Lozinka moze sadrzati samo slova i brojeve bez razmaka!');
                                event.preventDefault();
                                return false;
                            }
                            else if(trenutnaLozinka !== korisnik.lozinka){
                                alert("Uneli ste pogresnu trenutnu lozinku!");
                                event.preventDefault();
                                return false;
                            }
                            else if(novaLozinka == trenutnaLozinka){
                                alert("Nova lozinka se ne sme podudarati sa trenutnom!");
                                event.preventDefault();
                                return false;
                            }
                            else if(tipKorisnika == "Izaberi zeljeni tip"){
                                alert('Izaberite zeljeni tip korisnika');
                                event.preventDefault();
                                return false;
                            }
                            
                            params = {
                                    'action': 'izmenaKorisnika',
                                    'korimeKorisnik': korime,
                                    'lozinkaKorisnik': novaLozinka,
                                    'uloga' : tipKorisnika,
                                };
                            
                            $.post('ProfilServlet', params, function(data) {
                                if (data.status == 'unauthenticated') {
                                    window.location.replace('Login.html');
                                    return;
                                }
    
                                if (data.status == 'success') {
                                    window.location.replace('KorisnikProjekcije.html');
                                    return;
                                }
                            });
    
                            event.preventDefault();
                            return false;
                            }
    
                        });
                        
                        }else{
                            $('#adminIzmena').append(
                               
                            		'<div class="tabelaForma" style="margin-top:250px; margin-bottom:1600px">' +
                                    '<p>Izmena lozinke i tipa korisnika</p>' +
                                        '<table>' +
                                            '<tr><td>Lozinka:</td><td><input type="password" name="password" id="passwordInputAdmin"/></td></tr>' +
                                            '<tr><td>Tip Korisnika:</td><td><select id="tipKorisnika" name="comboBoxTipKorisnika">' +
                                                '<option value="7" selected="selected">Izaberi zeljeni tip</option>' +
                                                '<option value="1">KORISNIK</option>' +
                                                '<option value="2">ADMIN</option>' +		
                                            '</select></td></tr>' +
                                            '<tr><td/><td align="left"><input class="btn btn-success" type="submit" value="Potvrdi" id="izmenaPotvrdaSubmitAdmin"></td></tr>' +
                                        '</table>' +
                                    '</div>'
                            
                            );
                            
                            var novaLozinkaInput = $('#passwordInputAdmin');
                                            
                            $('#izmenaPotvrdaSubmitAdmin').on('click', function(event) {
                                var novaLozinka = novaLozinkaInput.val();
                                var tipKorisnika = $('#tipKorisnika option:selected').text();
                                
                                //console.log(trenutnaLozinka + novaLozinka);
                                
                                if(novaLozinka == ""){
                                    if(tipKorisnika == "Izaberi zeljeni tip"){
                                        alert('Izaberite zeljeni tip korisnika');
                                        event.preventDefault();
                                        return false;
                                    }
                                    novaLozinka = korisnik.lozinka;
                                    params = {
                                            'action': 'izmenaKorisnikaAdmin',
                                            'korimeKorisnik': korime,
                                            'lozinka': novaLozinka,
                                            'uloga' : tipKorisnika,
                                        };
                                    
                                    $.post('ProfilServlet', params, function(data) {
                                        if (data.status == 'unauthenticated') {
                                            window.location.replace('Login.html');
                                            return;
                                        }
    
                                        if (data.status == 'success') {
                                            window.location.replace('KorisnikProjekcije.html');
                                            return;
                                        }
                                    });
                                    event.preventDefault();
                                    return false;
                                }
                                else{
                                if (!novaLozinka.match(/^[0-9a-z]+$/i)){
                                    alert('Lozinka moze sadrzati samo slova i brojeve bez razmaka!');
                                    event.preventDefault();
                                    return false;
                                }
                                else if(tipKorisnika == "Izaberi zeljeni tip"){
                                    alert('Izaberite zeljeni tip korisnika');
                                    event.preventDefault();
                                    return false;
                                }
                                
                                params = {
                                        'action': 'izmenaKorisnikaAdmin',
                                        'korimeKorisnik': korime,
                                        'lozinka': novaLozinka,
                                        'uloga': tipKorisnika,
                                    };
                                
                                $.post('ProfilServlet', params, function(data) {
                                    if (data.status == 'unauthenticated') {
                                        window.location.replace('Login.html');
                                        return;
                                    }
    
                                    if (data.status == 'success') {
                                        window.location.replace('KorisnikGlavna.html');
                                        return;
                                    }
                                });
    
                                event.preventDefault();
                                return false;
                                }
    
                            });
                            
                        }
                        
                    });
                    
                    $('#brisanjeSubmit').on('click', function(event) {
                        params = {
                                'action': 'delete',
                                'korime': korime, 
                            };
                            $.post('ProfilServlet', params, function(data) {
                                if (data.status == 'unauthenticated') {
                                    window.location.replace('Login.html');
                                    return;
                                }
    
                                if (data.status == 'success') {
                                    window.location.replace('KorisnikGlavna.html');
                                    return;
                                }
                            });
    
                            event.preventDefault();
                            return false;
                        });
                    
                }
            }
        });
    }
    getKorisnik();
    
    });