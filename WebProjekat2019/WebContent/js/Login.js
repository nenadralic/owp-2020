$(document).ready(function() { 
	var usernameInput = $('#usernameInput');
    var passwordInput = $('#passwordInput');


	$('#loginSubmit').on('click', function(event) {
		var username = usernameInput.val();
		var password = passwordInput.val();
		console.log('username: ' + username);
		console.log('password: ' + password);		

		var params = {
			'username': username, 
			'password': password
		}
		$.post('PrijavaServlet', params, function(data) { 
			console.log('response')
			console.log(data);

			if (data.status == 'fail') {
				alert('Neispravni login podatci!')
				usernameInput.val('');
				passwordInput.val('');

				return;
			}
			if (data.status == 'success') {
				window.location.replace('KorisnikGlavna.html');
			}
		});
		
		event.preventDefault();
		return false;
	});
});
function myFunction() {
    var x = document.getElementById("passwordInput");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }

