package model;

public class Sala {
	private int id;
	private String naziv;
	private String tipProjekcije;
	private int brojSedista;
	
	
	
	public Sala() {
		
	}



	public Sala(int id, String naziv, String tipProjekcije, int brojSedista) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.tipProjekcije = tipProjekcije;
		this.brojSedista = brojSedista;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getNaziv() {
		return naziv;
	}



	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}



	public String getTipProjekcije() {
		return tipProjekcije;
	}



	public void setTipProjekcije(String tipProjekcije) {
		this.tipProjekcije = tipProjekcije;
	}



	public int getBrojSedista() {
		return brojSedista;
	}



	public void setBrojSedista(int brojSedista) {
		this.brojSedista = brojSedista;
	}



	
	
	

}
