package model;

public class Projekcije {
	
	private int id;
	private int film;
	private int tipProjekcije;
	private int sala;
	private String datumIVreme;
	private int cena;
	private String admin;
	private boolean aktivan;
	
	
	public Projekcije() {
		
		
	}


	public Projekcije(int id, int film, int tipProjekcije, int sala, String datumIVreme, int cena,
			String admin, boolean aktivan) {
		super();
		this.id = id;
		this.film = film;
		this.tipProjekcije = tipProjekcije;
		this.sala = sala;
		this.datumIVreme = datumIVreme;
		this.cena = cena;
		this.admin = admin;
		this.aktivan = aktivan;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getFilm() {
		return film;
	}


	public void setFilm(int film) {
		this.film = film;
	}


	public int getTipProjekcije() {
		return tipProjekcije;
	}


	public void setTipProjekcije(int tipProjekcije) {
		this.tipProjekcije = tipProjekcije;
	}


	public int getSala() {
		return sala;
	}


	public void setSala(int sala) {
		this.sala = sala;
	}


	public String getdatumIVreme() {
		return datumIVreme;
	}


	public void setdatumIVreme(String datumIVreme) {
		this.datumIVreme = datumIVreme;
	}


	public int getCena() {
		return cena;
	}


	public void setCena(int cena) {
		this.cena = cena;
	}


	public String getAdmin() {
		return admin;
	}


	public void setAdmin(String admin) {
		this.admin = admin;
	}


	public boolean isAktivan() {
		return aktivan;
	}


	public void setAktivan(boolean aktivan) {
		this.aktivan = aktivan;
	}


	
		

}