package model;

public class Film {
	
	private int id;
	private String naziv;
	private String reziser;
	private String glumci;
	private String zanrovi;
	private String trajanje;
	private String distributer;
	private String zemljaPorekla;
	private String godinaProizvodnje;
	private String opis;
	private boolean aktivan;
	
	
	public Film() {
	}


	public Film(int id, String naziv, String reziser, String glumci, String zanrovi, String trajanje,
			String distributer, String zemljaPorekla, String godinaProizvodnje, String opis, boolean aktivan) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.reziser = reziser;
		this.glumci = glumci;
		this.zanrovi = zanrovi;
		this.trajanje = trajanje;
		this.distributer = distributer;
		this.zemljaPorekla = zemljaPorekla;
		this.godinaProizvodnje = godinaProizvodnje;
		this.opis = opis;
		this.aktivan = aktivan;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNaziv() {
		return naziv;
	}


	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}


	public String getReziser() {
		return reziser;
	}


	public void setReziser(String reziser) {
		this.reziser = reziser;
	}


	public String getGlumci() {
		return glumci;
	}


	public void setGlumci(String glumci) {
		this.glumci = glumci;
	}


	public String getZanrovi() {
		return zanrovi;
	}


	public void setZanrovi(String zanrovi) {
		this.zanrovi = zanrovi;
	}


	public String getTrajanje() {
		return trajanje;
	}


	public void setTrajanje(String trajanje) {
		this.trajanje = trajanje;
	}


	public String getDistributer() {
		return distributer;
	}


	public void setDistributer(String distributer) {
		distributer = distributer;
	}


	public String getZemljaPorekla() {
		return zemljaPorekla;
	}


	public void setZemljaPorekla(String zemljaPorekla) {
		this.zemljaPorekla = zemljaPorekla;
	}


	public String getGodinaProizvodnje() {
		return godinaProizvodnje;
	}


	public void setGodinaProizvodnje(String godinaProizvodnje) {
		this.godinaProizvodnje = godinaProizvodnje;
	}


	public String getOpis() {
		return opis;
	}


	public void setOpis(String opis) {
		this.opis = opis;
	}


	public boolean isAktivan() {
		return aktivan;
	}


	public void setAktivan(boolean aktivan) {
		this.aktivan = aktivan;
	}


	
	
	
	

}
