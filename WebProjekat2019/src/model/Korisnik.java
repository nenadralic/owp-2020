package model;

public class Korisnik {
	
	private String korIme;
	private String lozinka;
	private String datumReg;
	private Uloga uloga;
	private boolean aktivan;
	
	public Korisnik() {
		
	}

	public Korisnik(String korIme, String lozinka, String datumReg, Uloga uloga, boolean aktivan) {
		super();
		this.korIme = korIme;
		this.lozinka = lozinka;
		this.datumReg = datumReg;
		this.uloga = uloga;
		this.aktivan = aktivan;
	}

	public String getKorIme() {
		return korIme;
	}

	public void setKorIme(String korIme) {
		this.korIme = korIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public String getDatumReg() {
		return datumReg;
	}

	public void setDatumReg(String datumReg) {
		this.datumReg = datumReg;
	}

	public Uloga getUloga() {
		return uloga;
	}

	public void setUloga(Uloga uloga) {
		this.uloga = uloga;
	}

	public boolean isAktivan() {
		return aktivan;
	}

	public void setAktivan(boolean aktivan) {
		this.aktivan = aktivan;
	}

	
	
	
	
	

}
