package model;

public class Karta {
	
	private int id;
	private int projekcija;
	private int sediste;
	private String datumIVreme;
	private String korisnik;
	
	
	public Karta() {
		
	}


	public Karta(int id, int projekcija, int sediste, String datumIVreme, String korisnik) {
		super();
		this.id = id;
		this.projekcija = projekcija;
		this.sediste = sediste;
		this.datumIVreme = datumIVreme;
		this.korisnik = korisnik;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getProjekcija() {
		return projekcija;
	}


	public void setProjekcija(int projekcija) {
		this.projekcija = projekcija;
	}


	public int getSediste() {
		return sediste;
	}


	public void setSediste(int sediste) {
		this.sediste = sediste;
	}


	public String getdatumIVreme() {
		return datumIVreme;
	}


	public void setdatumIVreme(String datumIVreme) {
		this.datumIVreme = datumIVreme;
	}


	public String getKorisnik() {
		return korisnik;
	}


	public void setKorisnik(String korisnik) {
		this.korisnik = korisnik;
	}


	
	
	
}
