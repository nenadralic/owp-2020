package model;

public class Sediste {
	private int redniBroj;
	private String sala;
	
	
	public Sediste() {
		super();
	}


	public Sediste(int redniBroj, String sala) {
		super();
		this.redniBroj = redniBroj;
		this.sala = sala;
	}


	public int getRedniBroj() {
		return redniBroj;
	}


	public void setRedniBroj(int redniBroj) {
		this.redniBroj = redniBroj;
	}


	public String getSala() {
		return sala;
	}


	public void setSala(String sala) {
		this.sala = sala;
	}
 
}
