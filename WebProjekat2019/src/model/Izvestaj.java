package model;

public class Izvestaj {

	private String nazivFilma;
	private int brProjekcija;
	private int brProdatihKarata;
	private int cena;
	
	public Izvestaj(String nazivFilma, int brProjekcija, int brProdatihKarata, int cena) {
		super();
		this.nazivFilma = nazivFilma;
		this.brProjekcija = brProjekcija;
		this.brProdatihKarata = brProdatihKarata;
		this.cena = cena;
	}

	public String getNazivFilma() {
		return nazivFilma;
	}

	public void setNazivFilma(String nazivFilma) {
		this.nazivFilma = nazivFilma;
	}

	public int getBrProjekcija() {
		return brProjekcija;
	}

	public void setBrProjekcija(int brProjekcija) {
		this.brProjekcija = brProjekcija;
	}

	public int getBrProdatihKarata() {
		return brProdatihKarata;
	}

	public void setBrProdatihKarata(int brProdatihKarata) {
		this.brProdatihKarata = brProdatihKarata;
	}

	public int getCena() {
		return cena;
	}

	public void setCena(int cena) {
		this.cena = cena;
	}
	
	
	
	
	
	
}
