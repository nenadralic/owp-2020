package baza;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.Korisnik;
import model.Uloga;

public class KorisnikDAO {

	public static List<Korisnik> getAll() {
		List<Korisnik> listaKor = new ArrayList<>();

		ConnectionManager.open();
		
		Connection conn = ConnectionManager.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		
		try {
			String query = "SELECT korIme, lozinka, datumReg, uloga, aktivan "
					+ "FROM listaKor WHERE aktivan = 1";

			pstmt = conn.prepareStatement(query);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				int index = 1;
				String korIme = rset.getString(index++);
				String lozinka = rset.getString(index++);
				String datumReg = rset.getString(index++);
				Uloga uloga = Uloga.valueOf(rset.getString(index++));
				boolean aktivan = rset.getBoolean(index++);

				Korisnik kor = new Korisnik(korIme, lozinka, datumReg, uloga, aktivan);

				listaKor.add(kor);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
			try {
				rset.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
			try {
				conn.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
		}

		return listaKor;
	}
	
	
	public static Korisnik get(String korIme, String lozinka) throws Exception {
		
		ConnectionManager.open();
		
		Connection conn = ConnectionManager.getConnection();
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {

			String query = "SELECT datumReg, uloga, aktivan FROM listaKor WHERE korIme = ? AND lozinka = ? AND aktivan = 1";
			
			pstmt = conn.prepareStatement(query);
			
			int index = 1;
			pstmt.setString(index++, korIme);
			pstmt.setString(index++, lozinka);
			
			rset = pstmt.executeQuery();

			if (rset.next()) {
				index = 1;
				String datumReg = rset.getString(index++);
				Uloga uloga = Uloga.valueOf(rset.getString(index++));
				boolean aktivan = rset.getBoolean(index++);

				return new Korisnik(korIme, lozinka, datumReg, uloga, aktivan);
			}
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}

		return null;
	}
	
	
	public static Korisnik get(String korIme) {

		ConnectionManager.open();
		
		Connection conn = ConnectionManager.getConnection();
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT korIme, lozinka, datumReg, uloga, aktivan FROM listaKor WHERE korIme = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, korIme);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				index = 2;
				String lozinka = rset.getString(index++);
				String datumReg = rset.getString(index++);
				Uloga uloga = Uloga.valueOf(rset.getString(index++));
				boolean aktivan = rset.getBoolean(index++);

				return new Korisnik(korIme, lozinka, datumReg, uloga, aktivan);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}

		return null;
	}
	
	
	public static Korisnik getPrijavljenKor(String korIme) {

		ConnectionManager.open();
		
		Connection conn = ConnectionManager.getConnection();
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT korIme, lozinka, datumReg, uloga, aktivan FROM listaKor WHERE korIme = ? AND aktivan = 1";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, korIme);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				index = 2;
				String lozinka = rset.getString(index++);
				String datumReg = rset.getString(index++);
				Uloga uloga = Uloga.valueOf(rset.getString(index++));
				boolean aktivan = rset.getBoolean(index++);

				return new Korisnik(korIme, lozinka, datumReg, uloga, aktivan);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}

		return null;
	}
	
	public static boolean dodajKorisnika(Korisnik korisnik) {
			
			ConnectionManager.open();
			
			Connection conn = ConnectionManager.getConnection();
	
			PreparedStatement pstmt = null;
			try {
				String query = "INSERT INTO listaKor (korIme, lozinka, datumReg, uloga, aktivan) "
						+ "VALUES (?, ?, ?, ?, ?)";
	
				pstmt = conn.prepareStatement(query);
				int index = 1;
	
				pstmt.setString(index++, korisnik.getKorIme());
				pstmt.setString(index++, korisnik.getLozinka());
				pstmt.setString(index++, korisnik.getDatumReg());
				pstmt.setString(index++, korisnik.getUloga().toString());
				pstmt.setBoolean(index++, korisnik.isAktivan());
	
				return pstmt.executeUpdate() == 1;
				
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
				try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
			}
	
			return false;
		}
		
	
	public static Korisnik getPrijavljenKorisnik(String korIme) {
	
		ConnectionManager.open();
		
		Connection conn = ConnectionManager.getConnection();
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT korIme, lozinka, datumReg, uloga, aktivan FROM listaKor WHERE korIme = ? AND aktivan = 1";
	
			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, korIme);
	
			rset = pstmt.executeQuery();
	
			if (rset.next()) {
				index = 2;
				String lozinka = rset.getString(index++);
				String datumReg = rset.getString(index++);
				Uloga uloga = Uloga.valueOf(rset.getString(index++));
				boolean aktivan = rset.getBoolean(index++);
	
				return new Korisnik(korIme, lozinka, datumReg, uloga, aktivan);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
	
		return null;
	}
	public static boolean izmenaKorisnika(Korisnik korisnik) {
		ConnectionManager.open();
		
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		try {
			String query = "UPDATE listaKor SET lozinka = ?, uloga = ? WHERE korIme = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, korisnik.getLozinka());
			pstmt.setString(index++, korisnik.getUloga().toString());
			pstmt.setString(index++, korisnik.getKorIme());

			return pstmt.executeUpdate() == 1;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}

		return false;
	}
	public static boolean brisanjeKorisnika(String korIme) {
		ConnectionManager.open();
		
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		try {
			String query = "DELETE FROM listaKor WHERE korIme = ?";

			int index = 1;
			pstmt = conn.prepareStatement(query);
			pstmt.setString(index++, korIme);

			return pstmt.executeUpdate() == 1;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}

		return false;
	}
	
	public static boolean logickoBrisanjeKorisnika(String korIme) {
		ConnectionManager.open();
		
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		try {
			String query = "UPDATE listaKor SET aktivan = 0 WHERE korIme = ? AND korIme IN (SELECT korisnik FROM karta)";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, korIme);

			return pstmt.executeUpdate() == 1;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}

		return false;
	}
	public static List<Korisnik> getKorime(String korime) {
		List<Korisnik> korisnici = new ArrayList<>();
		
		ConnectionManager.open();
		
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT korIme, lozinka, datumReg, uloga, aktivan FROM "
					+ "listaKor WHERE korIme LIKE ? and aktivan = 1";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, "%" + korime + "%");

			rset = pstmt.executeQuery();

			while (rset.next()) {
				index = 1;
				String korIme = rset.getString(index++);
				String lozinka = rset.getString(index++);
				String datumReg = rset.getString(index++);
				Uloga uloga = Uloga.valueOf(rset.getString(index++));
				boolean aktivan = rset.getBoolean(index++);

				Korisnik korisnik = new Korisnik(korIme, lozinka, datumReg, uloga, aktivan);
				
				korisnici.add(korisnik);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return korisnici;
	}
	
	public static List<Korisnik> getUloge(String uloga) {
		List<Korisnik> korisnici = new ArrayList<>();
		
		ConnectionManager.open();
		
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT korIme, lozinka, datumReg, uloga, aktivan FROM "
					+ "listaKor WHERE uloga = ? and aktivan = 1";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, uloga);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				index = 1;
				String korIme = rset.getString(index++);
				String lozinka = rset.getString(index++);
				String datumReg = rset.getString(index++);
				Uloga ulogaK = Uloga.valueOf(rset.getString(index++));
				boolean aktivan = rset.getBoolean(index++);

				Korisnik korisnik = new Korisnik(korIme, lozinka, datumReg, ulogaK, aktivan);
				
				korisnici.add(korisnik);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return korisnici;
	}
}


