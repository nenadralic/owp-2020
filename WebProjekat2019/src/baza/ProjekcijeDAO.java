package baza;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.Projekcije;

public class ProjekcijeDAO {
	public static List<Projekcije> getAll() {
		List<Projekcije> listaProjekcija = new ArrayList<>();

		ConnectionManager.open();
		
		Connection conn = ConnectionManager.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id, film, tipProjekcije, sala, datumIVreme, cena, admin, aktivan "
					+ "FROM projekcije WHERE aktivan = 1 AND (strftime('%d',datumIVreme) - strftime('%d','now'))=0 ORDER BY film, datumIVreme";

			pstmt = conn.prepareStatement(query);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				int film = rset.getInt(index++);
				int tipProjekcije = rset.getInt(index++);
				int sala = rset.getInt(index++);
				String datumIVreme = rset.getString(index++);
				int cena = rset.getInt(index++);
				String admin = rset.getString(index++);
				boolean aktivan = rset.getBoolean(index++);

				Projekcije projekcija = new Projekcije(id, film, tipProjekcije, sala, datumIVreme, cena, admin, aktivan);
				
				listaProjekcija.add(projekcija);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
			try {
				rset.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
			try {
				conn.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
		}

		return listaProjekcija;
	}
	
	public static List<Projekcije> getOpsegDatuma(String datumMin, String datumMax) {
		List<Projekcije> listaProjekcija = new ArrayList<>();
		
		ConnectionManager.open();
		
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id, film, tipProjekcije, sala, datumIVreme, cena, admin, aktivan FROM "
					+ "projekcije WHERE datumIVreme >= ? AND datumIVreme <= ? AND aktivan = 1";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++,datumMin);
			pstmt.setString(index++,datumMax);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				index = 1;
				int id = rset.getInt(index++);
				int film = rset.getInt(index++);
				int tipProjekcije = rset.getInt(index++);
				int sala = rset.getInt(index++);
				String datumIVreme = rset.getString(index++);
				int cena = rset.getInt(index++);
				String admin = rset.getString(index++);
				boolean aktivan = rset.getBoolean(index++);

				Projekcije projekcija = new Projekcije(id, film, tipProjekcije, sala, datumIVreme, cena, admin, aktivan);
				
				listaProjekcija.add(projekcija);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return listaProjekcija;
	}
	
	public static List<Projekcije> getOpsegCena(String cenaMin, String cenaMax) {
		List<Projekcije> listaProjekcija = new ArrayList<>();
		
		ConnectionManager.open();
		
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id, film, tipProjekcije, sala, datumIVreme, cena, admin, aktivan FROM "
					+ "projekcije WHERE CAST(cena AS double) >= ? and CAST(cena AS double) <= ? and aktivan = 1";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++,cenaMin);
			pstmt.setString(index++,cenaMax);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				index = 1;
				int id = rset.getInt(index++);
				int film = rset.getInt(index++);
				int tipProjekcije = rset.getInt(index++);
				int sala = rset.getInt(index++);
				String datumIVreme = rset.getString(index++);
				int cena = rset.getInt(index++);
				String admin = rset.getString(index++);
				boolean aktivan = rset.getBoolean(index++);

				Projekcije projekcija = new Projekcije(id, film, tipProjekcije, sala, datumIVreme, cena, admin, aktivan);
				
				listaProjekcija.add(projekcija);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return listaProjekcija;
	}
	
	public static List<Projekcije> getSala(String salaPretraga) {
		List<Projekcije> listaProjekcija = new ArrayList<>();
		
		ConnectionManager.open();
		
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT p.id, p.film, p.tipProjekcije, p.sala, p.datumIVreme, p.cena, p.admin, p.aktivan FROM "
					+ "projekcije p, sala s WHERE p.sala = s.id AND s.naziv LIKE ? AND (strftime('%d',p.datumIVreme) - strftime('%d','now'))=0";	 

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, "%" + salaPretraga + "%");

			rset = pstmt.executeQuery();

			while (rset.next()) {
				index = 1;
				int id = rset.getInt(index++);
				int film = rset.getInt(index++);
				int tipProjekcije = rset.getInt(index++);
				int sala = rset.getInt(index++);
				String datumIVreme = rset.getString(index++);
				int cena = rset.getInt(index++);
				String admin = rset.getString(index++);
				boolean aktivan = rset.getBoolean(index++);

				Projekcije projekcija = new Projekcije(id, film, tipProjekcije, sala, datumIVreme, cena, admin, aktivan);
				
				listaProjekcija.add(projekcija);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return listaProjekcija;
	}
	
	public static List<Projekcije> getTipProjekcije(String tipProjekcijeProjekcijePretraga) {
		List<Projekcije> listaProjekcija = new ArrayList<>();
		
		ConnectionManager.open();
		
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT p.id, p.film, p.tipProjekcije, p.sala, p.datumIVreme, p.cena, p.admin, p.aktivan FROM "
					+ "projekcije p, tipProjekcije t WHERE p.tipProjekcije = t.id AND t.naziv LIKE ? AND (strftime('%d',p.datumIVreme) - strftime('%d','now'))=0";	 

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, "%" + tipProjekcijeProjekcijePretraga + "%");

			rset = pstmt.executeQuery();

			while (rset.next()) {
				index = 1;
				int id = rset.getInt(index++);
				int film = rset.getInt(index++);
				int tipProjekcije = rset.getInt(index++);
				int sala = rset.getInt(index++);
				String datumIVreme = rset.getString(index++);
				int cena = rset.getInt(index++);
				String admin = rset.getString(index++);
				boolean aktivan = rset.getBoolean(index++);

				Projekcije projekcija = new Projekcije(id, film, tipProjekcije, sala, datumIVreme, cena, admin, aktivan);
				
				listaProjekcija.add(projekcija);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return listaProjekcija;
	}
	
	public static List<Projekcije> getNazivFilma(String nazivFilmaPretraga) {
		List<Projekcije> listaProjekcija = new ArrayList<>();
		
		ConnectionManager.open();
		
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT p.id, p.film, p.tipProjekcije, p.sala, p.datumIVreme, p.cena, p.admin, p.aktivan FROM "
					+ "projekcije p, film f WHERE p.film = f.id AND f.naziv LIKE ? AND (strftime('%d',datumIVreme) - strftime('%d','now'))=0";	 

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, "%" + nazivFilmaPretraga + "%");

			rset = pstmt.executeQuery();

			while (rset.next()) {
				index = 1;
				int id = rset.getInt(index++);
				int film = rset.getInt(index++);
				int tipProjekcije = rset.getInt(index++);
				int sala = rset.getInt(index++);
				String datumIVreme = rset.getString(index++);
				int cena = rset.getInt(index++);
				String admin = rset.getString(index++);
				boolean aktivan = rset.getBoolean(index++);

				Projekcije projekcija = new Projekcije(id, film, tipProjekcije, sala, datumIVreme, cena, admin, aktivan);
				
				listaProjekcija.add(projekcija);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return listaProjekcija;
	}
	public static List<Projekcije> getSve() {
		List<Projekcije> projekcije = new ArrayList<>();

		ConnectionManager.open();
		
		Connection conn = ConnectionManager.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id, film, tipProjekcije, sala, datumIVreme, cena, admin, aktivan "
					+ "FROM projekcije";

			pstmt = conn.prepareStatement(query);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				int film = rset.getInt(index++);
				int tipProjekcije = rset.getInt(index++);
				int sala = rset.getInt(index++);
				String datumIVreme = rset.getString(index++);
				int cena = rset.getInt(index++);
				String admin = rset.getString(index++);
				boolean aktivan = rset.getBoolean(index++);

				Projekcije projekcija = new Projekcije(id, film, tipProjekcije, sala, datumIVreme, cena, admin, aktivan);
				
				projekcije.add(projekcija);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
			try {
				rset.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
			try {
				conn.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
		}

		return projekcije;
	}
	public static Projekcije get(String idProjekcije) {

		ConnectionManager.open();
		
		Connection conn = ConnectionManager.getConnection();
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id, film, tipProjekcije, sala, datumIVreme, cena, admin, aktivan FROM projekcije WHERE id = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, idProjekcije);

			rset = pstmt.executeQuery();
			
			int id = Integer.parseInt(idProjekcije);

			if (rset.next()) {
				index = 2;
				int film = rset.getInt(index++);
				int tipProjekcije = rset.getInt(index++);
				int sala = rset.getInt(index++);
				String datumIVreme = rset.getString(index++);
				int cena = rset.getInt(index++);
				String administrator = rset.getString(index++);
				boolean aktivan = rset.getBoolean(index++);

				return new Projekcije(id, film, tipProjekcije, sala, datumIVreme, cena, administrator, aktivan);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}

		return null;
	}
	public static boolean brisanjeProjekcije(String idProjekcije) {
		ConnectionManager.open();
		
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		try {
			String query = "DELETE FROM projekcije WHERE id = ?";

			int index = 1;
			pstmt = conn.prepareStatement(query);
			pstmt.setString(index++, idProjekcije);

			return pstmt.executeUpdate() == 1;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}

		return false;
	}
	
	public static boolean logickoBrisanjeProjekcije(String idProjekcije) {
		ConnectionManager.open();
		
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		try {
			String query = "UPDATE projekcije SET aktivan = 0 WHERE id = ? AND id IN (SELECT projekcija FROM karta)";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, idProjekcije);

			return pstmt.executeUpdate() == 1;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}

		return false;
	}
public static boolean add(Projekcije projekcija) {
		
		ConnectionManager.open();
		
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		try {
			String query = "INSERT INTO projekcije(film,tipProjekcije,sala,datumIVreme,cena,admin,aktivan) "
					+ "VALUES (?, ?, ?, ?, ?, ?, ?)";

			pstmt = conn.prepareStatement(query);
			int index = 1;

			pstmt.setInt(index++, projekcija.getFilm());
			pstmt.setInt(index++, projekcija.getTipProjekcije());
			pstmt.setInt(index++, projekcija.getSala());
			pstmt.setString(index++, projekcija.getdatumIVreme());
			pstmt.setDouble(index++, projekcija.getCena());
			pstmt.setString(index++, projekcija.getAdmin());
			pstmt.setBoolean(index++, projekcija.isAktivan());
			

			return pstmt.executeUpdate() == 1;
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}

		return false;
	}
	

}
